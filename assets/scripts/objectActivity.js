const bankAccount = {
	name: "Brandon Cruz",
	accountNo: "123-456",
	accountBal: 5000,
	showCustomer: function(){
		return "Customer Name: " + bankAccount.name;
	},
	showAccountNumber: function(){
		return "Customer Account Number: " + bankAccount.accountNo;
	},
	showCustomerDetails: function(){
		return "Customer Name: " + bankAccount.name + " " + "Customer Account Number: " 
		+ bankAccount.accountNo + " " + "Balance: " + bankAccount.accountBal;
	},
	showBalance: function(){
		return "Account Balance: " + bankAccount.accountBal;
	},
	deposit: function(amount){
		bankAccount.accountBal += amount;
		return "Remaining Balance: " + bankAccount.accountBal;
	},
	withdraw: function(amount){
		bankAccount.accountBal -= amount;
		return "Remaining Balance: " + bankAccount.accountBal;
	}
};

// const amount = 300;

//Concatenation - String to String, string to number, normally if we are printing variables
// 1.Create the following key-value pairs:
// A. name - "Brandon Cruz",
// B. accountNo. - "123-456",
// C. accountBal - 5000

// 2. Create the following methods:
// A. showCustomer:
// 	Must return: "Customer Name: " 
// B. showAccountNumber:
// 	Must return: "Customer Account Number: 123-456"
// C. showCutomerDetails:
// 	Must return: "Customer Name: Brandon Cruz, Customer Account Number: 123-456, Balance: 5000";
// D. showBalance:
// 	Must return: "Account Balance: 5000";
// E. deposit:
// 	--> accepts one parameter (amount) and adds the amount to the accountBal.
// F. withdraw:
// 	--> accepts one parameter (amount) and subtracts athe amount from the accountBal.