
function getSum(num1, num2) {
	const sum = num1 + num2;
	return sum;
}

function getDifference(num3, num4) {
	const difference = num3 - num4;
	return difference;
}

function getProduct(num1, num2) {
	const prod = num1 * num2;
	return prod;
}


function getQuotient(num1, num2) {
	const quotient = num1 / num2;
	return quotient;
}

function getQuotient(num1, num2) {
	const quotient = num1 / num2;
	return quotient;
}

function getCylinderVolume(radius, height) {
	const cvolume = Math.PI * radius * radius * height;
	return cvolume;
}

function getTrapezoidArea(base1, base2, height) {
	const barea = base1 + base2;
	// return barea;
	const trap = barea / 2; 
	// return trap;
	const tarea = trap * height;
	return tarea;
}

function getTotalAmount(cost, quantity, disamt, amtax) {
	const value = cost + quantity;
	// return value;
	const dvalue = value * disamt;
	// return dvalue;
	const fvalue = dvalue + amtax;
	return fvalue;
}

function getTotalSalary(month, workd, numda, minlate, amtaxa) {
	const deduct = numda + minlate;
	// return deduct;
	const salary = workd - deduct;
	// return salary;
	const gsalary = month / salary;
	const nsalary = gsalary - amtax;
	
	return nsalary; 
}